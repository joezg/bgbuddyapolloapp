import React from 'react';
import { Route } from 'react-router-dom';

import Buddies from './Buddies';
import Games from './Games';
import Locations from './Locations';
import Matches from './Matches';

export default ({ match }) => {
  return (
    <>
      <Route path={`${match.url}/buddies`} component={Buddies} />
      <Route path={`${match.url}/games`} component={Games} />
      <Route path={`${match.url}/locations`} component={Locations} />
      <Route path={`${match.url}/matches`} component={Matches} />
    </>
  );
};
