import React from 'react';
import { Route } from 'react-router-dom';
import { Games, AddGame, EditGame, ViewGame } from '../containers';

class GamesPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: ''
    };
  }

  handleUpdateSearchQuery = query => {
    this.setState({
      searchQuery: query
    });
  };

  handleDetailsClicked = _id => {
    this.props.history.push(`${this.props.match.url}/view/${_id}`);
  };

  handleNewItemClicked = () => {
    this.props.history.push(`${this.props.match.url}/add`);
  };

  handleSubmitComplete = () => {
    this.props.history.goBack();
  };

  handleEditItemClicked = _id => {
    this.props.history.push(`${this.props.match.url}/edit/${_id}`);
  };

  render() {
    return (
      <>
        <Route
          exact
          path={this.props.match.url}
          render={() => (
            <Games
              onUpdateSearchQuery={this.handleUpdateSearchQuery}
              onDetailsClicked={this.handleDetailsClicked}
              searchQuery={this.state.searchQuery}
              onNewItemClicked={this.handleNewItemClicked}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/add`}
          render={() => (
            <AddGame onSubmitComplete={this.handleSubmitComplete} />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/edit/:id`}
          render={props => (
            <EditGame
              onSubmitComplete={this.handleSubmitComplete}
              id={props.match.params.id}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/view/:id`}
          render={props => (
            <ViewGame
              onEditItemClicked={this.handleEditItemClicked}
              id={props.match.params.id}
            />
          )}
        />
      </>
    );
  }
}

export default GamesPage;
