import React from 'react';
import { Route } from 'react-router-dom';
import { Buddies, AddBuddy, EditBuddy, ViewBuddy } from '../containers';

class BuddiesPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: ''
    };
  }

  handleUpdateSearchQuery = query => {
    this.setState({
      searchQuery: query
    });
  };

  handleDetailsClicked = _id => {
    this.props.history.push(`${this.props.match.url}/view/${_id}`);
  };

  handleNewItemClicked = () => {
    this.props.history.push(`${this.props.match.url}/add`);
  };

  handleSubmitComplete = () => {
    this.props.history.goBack();
  };

  handleEditItemClicked = _id => {
    this.props.history.push(`${this.props.match.url}/edit/${_id}`);
  };

  render() {
    return (
      <>
        <Route
          exact
          path={this.props.match.url}
          render={() => (
            <Buddies
              onUpdateSearchQuery={this.handleUpdateSearchQuery}
              onDetailsClicked={this.handleDetailsClicked}
              searchQuery={this.state.searchQuery}
              onNewItemClicked={this.handleNewItemClicked}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/add`}
          render={() => (
            <AddBuddy onSubmitComplete={this.handleSubmitComplete} />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/edit/:id`}
          render={props => (
            <EditBuddy
              onSubmitComplete={this.handleSubmitComplete}
              id={props.match.params.id}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/view/:id`}
          render={props => (
            <ViewBuddy
              onEditItemClicked={this.handleEditItemClicked}
              id={props.match.params.id}
            />
          )}
        />
      </>
    );
  }
}

export default BuddiesPage;
