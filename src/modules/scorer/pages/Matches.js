import React from 'react';
import { Route } from 'react-router-dom';
import {
  Matches,
  AddMatch,
  ViewMatch,
  EditMatch,
  AddGame,
  AddBuddy,
  AddLocation
} from '../containers';

class MatchesPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: '',
      currentEditMatch: this.getNewEditMatch,
      searchQueries: {
        buddies: '',
        games: '',
        locations: ''
      }
    };
  }

  getNewEditMatch = () => ({
    players: [],
    date: new Date()
  });

  resetEditMatch = () => {
    this.setState({
      currentEditMatch: this.getNewEditMatch()
    });
  };

  handleUpdateSearchQuery = searchQuery => {
    this.setState({
      searchQuery
    });
  };

  handleUpdateSearchQueries = query => {
    this.setState(state => ({
      searchQueries: {
        ...state.searchQueries,
        ...query
      }
    }));
  };

  handleDetailsClicked = _id => {
    this.props.history.push(`${this.props.match.url}/view/${_id}`);
  };

  handleNewItemClicked = () => {
    this.resetEditMatch();
    this.props.history.push(`${this.props.match.url}/add`);
  };

  handleSubmitComplete = item => {
    item &&
      item.buddy &&
      this.setState(state => ({
        currentEditMatch: {
          ...state.currentEditMatch,
          players: [
            ...state.currentEditMatch.players,
            { ...item.buddy, result: 0, rank: '', winner: false }
          ]
        }
      }));

    item &&
      (item.game || item.location) &&
      this.handleOnCurrentEditMatchChanged(item);

    this.props.history.goBack();
  };

  handleEditItemClicked = _id => {
    this.props.history.push(`${this.props.match.url}/edit/${_id}`);
  };

  handleAddNewItem = itemName => {
    this.props.history.push(`${this.props.match.url}/${itemName}/add`);
  };

  handleOnCurrentEditMatchChanged = newState => {
    this.setState(oldState => ({
      currentEditMatch: {
        ...oldState.currentEditMatch,
        ...newState
      }
    }));
  };

  render() {
    return (
      <>
        <Route
          exact
          path={this.props.match.url}
          render={() => (
            <Matches
              onUpdateSearchQuery={this.handleUpdateSearchQuery}
              onDetailsClicked={this.handleDetailsClicked}
              searchQuery={this.state.searchQuery}
              onNewItemClicked={this.handleNewItemClicked}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/add`}
          render={() => (
            <AddMatch
              onSubmitComplete={this.handleSubmitComplete}
              currentEditMatch={this.state.currentEditMatch}
              searchQueries={this.state.searchQueries}
              onUpdateSearchQueries={this.handleUpdateSearchQueries}
              onAddNewItem={this.handleAddNewItem}
              onCurrentEditMatchChanged={this.handleOnCurrentEditMatchChanged}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/edit/:id`}
          render={props => (
            <EditMatch
              id={props.match.params.id}
              onSubmitComplete={this.handleSubmitComplete}
              currentEditMatch={this.state.currentEditMatch}
              searchQueries={this.state.searchQueries}
              onUpdateSearchQueries={this.handleUpdateSearchQueries}
              onAddNewItem={this.handleAddNewItem}
              onCurrentEditMatchChanged={this.handleOnCurrentEditMatchChanged}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/view/:id`}
          render={props => (
            <ViewMatch
              onEditItemClicked={this.handleEditItemClicked}
              id={props.match.params.id}
            />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/games/add`}
          render={props => (
            <AddGame onSubmitComplete={this.handleSubmitComplete} />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/buddies/add`}
          render={props => (
            <AddBuddy onSubmitComplete={this.handleSubmitComplete} />
          )}
        />
        <Route
          exact
          path={`${this.props.match.url}/locations/add`}
          render={props => (
            <AddLocation onSubmitComplete={this.handleSubmitComplete} />
          )}
        />
      </>
    );
  }
}

export default MatchesPage;
