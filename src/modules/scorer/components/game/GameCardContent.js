import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';

class GameCardContent extends Component {
  render() {
    const item = this.props.item;
    
    const playedNumberTimes = <span>played <strong>{item.matchCount + item.previousPlaysNumber}</strong> {item.matchCount === 1 ? ' time' : ' times' }</span>;
    
    let dateFirstPlayed = null;
    if (item.previousPlaysNumber){
      dateFirstPlayed = "in the past"
    } else if (item.dateFirstPlayed) {
      dateFirstPlayed = item.dateFirstPlayed.relative
    }

    let dateLastPlayed = null;
    if (item.dateLastPlayed) {
      dateLastPlayed = item.dateLastPlayed.relative
    } else if (item.previousPlaysNumber){
      dateLastPlayed = "in the past"
    } 

    return (
      <Card.Content>
        <Card.Header>
          {item.name}
        </Card.Header>
        <Card.Meta>
          {item.owned ? "Owned" : "Not owned"}
          {item.owned !== item.played ? ", but " : " and "}
          {item.played ? playedNumberTimes : "not played"}
        </Card.Meta>
        <Card.Description>
          {dateFirstPlayed ? <div>First time played: {dateFirstPlayed}</div> : null }
          {dateLastPlayed ? <div>Last time played: {dateLastPlayed}</div> : null }
          {item.wantToPlay && item.played ? <strong>I want to play this game again</strong> : null}
          {item.wantToPlay && !item.played ? <strong>I want to play this game</strong> : null}
        </Card.Description>
      </Card.Content>
    );
  }
}

export default GameCardContent;