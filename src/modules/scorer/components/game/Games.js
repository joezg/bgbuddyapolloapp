import React from 'react';
import GameCardContent from './GameCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import { Container, Header } from 'semantic-ui-react';

const Games = props => {
  return (
    <Container>
      <Header as="h3" block>
        Games of interest
      </Header>
      {props.games ? (
        <ItemCards
          items={props.games}
          cardContentComponent={GameCardContent}
          hasMore={props.games.length < props.gameCount}
          onLoadMoreClick={props.onLoadMore}
          onDeleteItemClick={props.onDeleteGame}
          onQueryChanged={props.onUpdateSearchQuery}
          query={props.searchQuery}
          onDetailsClicked={props.onDetailsClicked}
          onNewItemClicked={props.onNewItemClicked}
          canDelete={item => item.matchCount === 0}
        />
      ) : null}
    </Container>
  );
};

export default Games;
