import React, { Component, Fragment } from 'react';
import { Segment, Header } from 'semantic-ui-react';
import MatchCardContent from '../match/MatchCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import FloatingButton from '../../../../components/common/FloatingButton';

class GamePage extends Component {
  render() {
    const game = this.props.game;
    let playedNumberTimes = '';
    if (game) {
      playedNumberTimes = (
        <span>
          You played{' '}
          <strong>
            {game.matchCount + game.previousPlaysNumber}{' '}
            {game.matchCount === 1 ? 'match' : 'matches'}
          </strong>{' '}
          of this game.
        </span>
      );
    }

    return (
      <Fragment>
        <Header size="large" content={game ? game.name : '...'} block />
        {game ? (
          <Segment raised>
            <p>
              You <strong>{game.owned ? 'own' : 'do not own'}</strong> this
              game.
            </p>
            <p>
              You <strong>{game.wantToPlay ? 'want' : 'do not want'}</strong> to
              play this game.
            </p>
            <p>{playedNumberTimes}</p>
            <Header size="medium" content="Matches" block />
            <Segment>
              <ItemCards
                items={game.matches}
                cardContentComponent={MatchCardContent}
                onDeleteItemClick={this.props.onDeleteMatch}
                detailsPath="/match/"
                noItemsMessage="No recorded matches played"
              />
            </Segment>
            <FloatingButton
              onClick={this.props.onEditItemClicked}
              iconName="edit"
            />
          </Segment>
        ) : null}
      </Fragment>
    );
  }
}

export default GamePage;
