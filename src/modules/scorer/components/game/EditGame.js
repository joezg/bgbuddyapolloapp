import React, { Component } from 'react';
import ActionForm from '../../../../components/common/ActionForm';

class EditGame extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bggSearchResult: []
    };

    this.handleBggSearchClicked = this.handleBggSearchClicked.bind(this);
  }

  render() {
    return (
      <ActionForm
        item={this.props.game}
        fields={[
          {
            type: 'id',
            name: '_id'
          },
          {
            type: 'bggSearch',
            name: 'name',
            placeholder: 'Enter game name',
            label: 'Enter game name',
            serachResult: this.state.bggSearchResult,
            onBggSearchClicked: this.handleBggSearchClicked
          },
          {
            type: 'id',
            name: 'bggId'
          },
          {
            type: 'switch',
            name: 'owned',
            label: 'Owned?'
          },
          {
            type: 'switch',
            name: 'played',
            label: 'Played?'
          },
          {
            type: 'input',
            name: 'previousPlaysNumber',
            placeholder: 'Number of plays',
            label: 'Enter number of plays',
            visible: ({ played }) => {
              return played;
            }
          },
          {
            type: 'switch',
            name: 'wantToPlay',
            label: 'Want to play?'
          }
        ]}
        formHeader={
          this.props.game ? `Edit "${this.props.game.name}"` : 'New Game'
        }
        onSubmit={this.props.onSubmitGame}
      />
    );
  }

  async handleBggSearchClicked(bggSearchQuery) {
    if (null === bggSearchQuery) {
      return this.setState({ bggSearchResult: [] });
    }

    const result = await this.props.onBggSearch(bggSearchQuery);
    this.setState({
      bggSearchResult: result
    });
  }
}

export default EditGame;
