import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';

class LocationCardContent extends Component {
  render() {
    const item = this.props.item;
    
    const playedNumberTimes = <span><strong>{item.matchCount}</strong> {item.matchCount === 1 ? ' match' : ' matches' } played at this location</span>;
    const differentBuddies = <span><strong>{item.buddyCount}</strong> {item.matchCount === 1 ? ' buddy' : ' buddies' } played at this location</span>;
    
    return (
      <Card.Content>
        <Card.Header>
          {item.name}
        </Card.Header>
        <Card.Meta>
          {playedNumberTimes}
          <br />
          {differentBuddies}
        </Card.Meta>
        <Card.Description>
          
        </Card.Description>
      </Card.Content>
    );
  }
}

export default LocationCardContent;
