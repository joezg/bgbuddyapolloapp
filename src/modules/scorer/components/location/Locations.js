import React from 'react';
import LocationCardContent from './LocationCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import { Container, Header } from 'semantic-ui-react';

const Locations = (props) => {
    return ( 
        <Container>
            <Header as='h3' block>
                My playing locations
            </Header>
            { props.locations ? 
                <ItemCards 
                    items={props.locations} 
                    cardContentComponent={LocationCardContent}
                    hasMore={props.locations.length < props.locationCount}
                    onLoadMoreClick={props.onLoadMore}
                    onDeleteItemClick={props.onDeleteLocation}
                    onQueryChanged={props.onUpdateSearchQuery} 
                    query={props.searchQuery}
                    onDetailsClicked={props.onDetailsClicked}
                    onNewItemClicked={props.onNewItemClicked}
                    canDelete={(item)=>item.matchCount === 0}
                />
                : null }
        </Container>
    );
}

export default Locations;
