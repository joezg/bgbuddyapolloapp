import React from 'react';
import ActionForm from '../../../../components/common/ActionForm';

const EditLocation = props => {
  return (
    <ActionForm
      item={props.location}
      fields={[
        {
          type: 'id',
          name: '_id'
        },
        {
          type: 'input',
          name: 'name',
          placeholder: 'Enter location name',
          label: 'Enter location name'
        }
      ]}
      formHeader={
        props.location ? `Edit "${props.location.name}"` : 'New Location'
      }
      onSubmit={props.onSubmitLocation}
    />
  );
};

export default EditLocation;
