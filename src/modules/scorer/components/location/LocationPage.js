import React, { Component } from 'react';
import { Segment, Header } from 'semantic-ui-react';
import MatchCardContent from '../match/MatchCardContent';
import BuddyCardContent from '../buddy/BuddyCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import FloatingButton from '../../../../components/common/FloatingButton';

class LocationPage extends Component {
  render() {
    const location = this.props.location;

    return (
      <div>
        <Header size="large" content={location ? location.name : '...'} block />
        {location ? (
          <Segment raised>
            <p>
              You played <strong>{location.matchCount} matches </strong> on this
              location.
            </p>
            <p>
              There are <strong>{location.buddyCount} buddies </strong> who
              played on this location.
            </p>
            <Header size="medium" content="Buddies" block />
            <Segment>
              <ItemCards
                items={location.buddies}
                cardContentComponent={BuddyCardContent}
                canDelete={false}
                actions={{ remove: false }}
                detailsPath="/buddy/"
              />
              <FloatingButton
                onClick={this.props.onEditItemClicked}
                iconName="edit"
              />
            </Segment>
            <Header size="medium" content="Matches" block />
            <Segment>
              <ItemCards
                items={location.matches}
                cardContentComponent={MatchCardContent}
                onDeleteItemClick={this.props.onDeleteMatch}
                detailsPath="/match/"
              />
            </Segment>
          </Segment>
        ) : null}
      </div>
    );
  }
}

export default LocationPage;
