import React from 'react';
import { Menu, Icon } from 'semantic-ui-react';

export default ({ history, match, location }) => {
  const handleRouteClick = url => {
    history.push(url);
  };
  return (
    <>
      <Menu.Item
        active={location.pathname === `${match.url}/matches`}
        as="a"
        onClick={() => handleRouteClick(`${match.url}/matches`)}
      >
        <Icon name="trophy" />
        Matches
      </Menu.Item>
      <Menu.Item
        active={location.pathname === `${match.url}/games`}
        as="a"
        onClick={() => handleRouteClick(`${match.url}/games`)}
      >
        <Icon name="gamepad" />
        Games
      </Menu.Item>
      <Menu.Item
        active={location.pathname === `${match.url}/buddies`}
        as="a"
        onClick={() => handleRouteClick(`${match.url}/buddies`)}
      >
        <Icon name="users" />
        Buddies
      </Menu.Item>
      <Menu.Item
        active={location.pathname === `${match.url}/locations`}
        as="a"
        onClick={() => handleRouteClick(`${match.url}/locations`)}
      >
        <Icon name="point" />
        Locations
      </Menu.Item>
    </>
  );
};
