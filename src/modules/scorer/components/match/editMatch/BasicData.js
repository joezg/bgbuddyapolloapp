import React, { Component, Fragment } from 'react';
import { Icon } from 'semantic-ui-react';
import ChooseItem from './ChooseItem';
import DatePicker from 'react-datepicker';
import styles from '../EditMatch.module.scss';

class BasicData extends Component {
  render() {
    return (
      <Fragment>
        {this.props.games && (
          <ChooseItem
            headerIcon="volleyball ball"
            notChosenLabel="No game chosen"
            items={this.props.games}
            onLoadMoreClick={this.props.loadMoreGames}
            hasMore={this.props.games.length < this.props.gameCount}
            onQueryChanged={query =>
              this.props.onUpdateSearchQueries({ games: query })
            }
            itemListQuery={this.props.searchQueries.games}
            onSelected={this.props.onGameSelected}
            chosen={this.props.currentEditMatch.game}
            onAddNewItem={() => this.props.onAddNewItem('games')}
          />
        )}
        {this.props.locations && (
          <ChooseItem
            headerIcon="thumbtack"
            notChosenLabel="No location chosen"
            items={this.props.locations}
            onLoadMoreClick={this.props.loadMoreLocations}
            hasMore={this.props.locations.length < this.props.locationCount}
            onQueryChanged={query =>
              this.props.onUpdateSearchQueries({ locations: query })
            }
            itemListQuery={this.props.searchQueries.locations}
            onSelected={this.props.onLocationSelected}
            chosen={this.props.currentEditMatch.location}
            onAddNewItem={() => this.props.onAddNewItem('locations')}
          />
        )}

        <section className={styles.editMatchElement}>
          <Icon name="calendar" size="big" />
          <div className={styles.datePicker}>
            {
              <DatePicker
                selected={new Date(this.props.currentEditMatch.date)}
                showTimeSelect
                dateFormat="dd.MM.yyyy. HH:mm"
                timeFormat="HH:mm"
                onChange={this.props.onDateChange}
              />
            }
          </div>
        </section>
      </Fragment>
    );
  }
}

export default BasicData;
