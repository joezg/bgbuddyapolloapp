import React, { Component } from 'react';
import { Label, Icon, Button, Card, Grid, Segment } from 'semantic-ui-react';
import ItemCards from '../../../../../components/common/ItemCards';
import styles from '../EditMatch.module.scss';
import classnames from 'classnames';

const CardContent = ({ item }) => {
  return (
    <Card.Content>
      <Card.Header>{item.name}</Card.Header>
      <Card.Description>{item.isPicked ? 'Picked!' : ''}</Card.Description>
    </Card.Content>
  );
};

class ChooseMultipleItems extends Component {
  constructor(props) {
    super(props);

    this.state = {
      picking: false
    };

    this.handlePickClick = this.handlePickClick.bind(this);
    this.handleDoneClick = this.handleDoneClick.bind(this);
  }

  render() {
    const pickedIdField = this.props.pickedIdField || '_id';

    const items = this.props.items
      ? this.props.items.map(item => {
          return {
            ...item,
            isPicked: !!this.props.picked.find(picked => {
              return picked[pickedIdField] === item._id;
            })
          };
        })
      : [];

    return (
      <>
        <Segment basic className={styles.editMatchElement}>
          <Grid columns={3} verticalAlign="middle" stackable>
            <Grid.Row>
              <Grid.Column width={1}>
                <Icon className={this.props.icon} size="big" />
              </Grid.Column>
              <Grid.Column width={8}>
                {this.props.picked.length} {this.props.itemNamePlural} picked
              </Grid.Column>
              <Grid.Column width={7}>
                {!this.state.picking && (
                  <Button basic color="black" onClick={this.handlePickClick}>
                    Pick
                  </Button>
                )}
                {this.state.picking && (
                  <Button basic color="black" onClick={this.handleDoneClick}>
                    Done
                  </Button>
                )}
                {this.state.picking && (
                  <Button onClick={this.props.onAddNewItem} basic color="black">
                    New
                  </Button>
                )}
                {this.state.picking && this.props.onCustomAddClick && (
                  <Button
                    basic
                    color="black"
                    onClick={this.props.onCustomAddClick}
                  >
                    {this.props.customAddLabel}
                  </Button>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        {this.props.picked.length > 0 && (
          <Segment
            basic
            className={classnames(styles.editMatchElement, styles.pickedItems)}
          >
            {this.props.picked.map((item, i) => {
              return (
                <Label key={item._id || i} size="big" className={styles.player}>
                  <div className={styles.icon}>
                    <Icon className={this.props.icon} />
                  </div>
                  <div className={styles.name}>
                    <span>{item.name}</span>
                  </div>
                  <div className={styles.remove}>
                    <Icon
                      className="delete"
                      color="red"
                      onClick={e => this.props.onItemRemove(item)}
                    />
                  </div>
                </Label>
              );
            })}
          </Segment>
        )}
        {this.state.picking && (
          <Segment basic>
            <ItemCards
              items={items}
              canDelete={false}
              hasMore={this.props.items.length < this.props.itemsCount}
              cardContentComponent={CardContent}
              actions={{ select: true, remove: false, details: false }}
              onLoadMoreClick={this.props.onLoadMoreClick}
              onQueryChanged={this.props.onQueryChanged}
              query={this.props.searchQuery}
              onSelected={this.props.onItemSelected}
              hideCardActions={item => item.isPicked}
            />
          </Segment>
        )}
      </>
    );
  }

  handlePickClick() {
    this.setState({
      picking: true
    });
  }

  handleDoneClick() {
    this.setState({
      picking: false
    });
  }
}

export default ChooseMultipleItems;
