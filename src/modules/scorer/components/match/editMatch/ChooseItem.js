import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import ItemCards from '../../../../../components/common/ItemCards';
import { Button, Icon } from 'semantic-ui-react';
import styles from '../EditMatch.module.scss';

const CardContent = ({ item }) => {
  return (
    <Card.Content>
      <Card.Header>{item.name}</Card.Header>
    </Card.Content>
  );
};

export default class ChooseItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      choosing: !!props.chosen
    };

    this.handleChooseClick = this.handleChooseClick.bind(this);
    this.handleChangeClick = this.handleChangeClick.bind(this);
  }

  render() {
    return (
      <div className={styles.editMatchElement}>
        <Icon className={this.props.headerIcon} size="big" />{' '}
        {!this.props.chosen && <span>{this.props.notChosenLabel}</span>}
        {this.props.chosen && <span>{this.props.chosen.name}</span>}
        {!this.props.chosen && !this.state.choosing && (
          <Button
            floated="right"
            basic
            color="black"
            onClick={this.handleChooseClick}
          >
            Choose
          </Button>
        )}
        {this.props.chosen && (
          <Button
            floated="right"
            basic
            color="black"
            onClick={this.handleChangeClick}
          >
            Change
          </Button>
        )}
        {!this.props.chosen && this.state.choosing && (
          <Button
            onClick={this.props.onAddNewItem}
            floated="right"
            basic
            color="black"
          >
            New
          </Button>
        )}
        {!this.props.chosen && this.state.choosing && (
          <ItemCards
            items={this.props.items}
            canDelete={false}
            hasMore={this.props.hasMore}
            cardContentComponent={CardContent}
            actions={{ select: true, remove: false, details: false }}
            onLoadMoreClick={this.props.onLoadMoreClick}
            onQueryChanged={this.props.onQueryChanged}
            query={this.props.itemListQuery}
            onSelected={this.props.onSelected}
          />
        )}
      </div>
    );
  }

  handleChooseClick() {
    this.setState({
      choosing: true
    });
  }
  handleChangeClick() {
    this.props.onSelected(null);
    this.setState({
      choosing: false
    });
  }
}
