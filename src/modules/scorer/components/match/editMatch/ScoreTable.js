import React, { Component } from 'react';
import { Table, Button, Label, Icon, Input } from 'semantic-ui-react';

export default class ScoreTable extends Component {
  constructor(props) {
    super(props);

    this.handleValueChange = this.handleValueChange.bind(this);
  }

  handleValueChange(evt, buddyId) {
    const value = evt.target.value;
    const newScore = this.props.players.map(player => ({
      ...player
    }));

    let changed = newScore.find(player => player.buddyId === buddyId);

    if (value === '') {
      changed.result = value;
    } else {
      changed.result = +value;
    }

    [...newScore]
      .sort((a, b) => b.result - a.result)
      .forEach((score, i) => {
        score.rank = i + 1;
        score.winner = i === 0;
      });

    this.props.onScoreChanged(newScore);
  }

  render() {
    return (
      <Table celled unstackable color="teal">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Player</Table.HeaderCell>
            <Table.HeaderCell>Score</Table.HeaderCell>
            <Table.HeaderCell>Rank</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {this.props.players.map((player, i) => {
            return (
              <Table.Row key={player._id || i}>
                <Table.Cell>
                  {!!player.winner && (
                    <Label ribbon>
                      <Icon name="trophy" />
                    </Label>
                  )}
                  {player.name}
                </Table.Cell>
                <Table.Cell>
                  <Input
                    placeholder="result"
                    type="number"
                    value={player.result}
                    onChange={ev => this.handleValueChange(ev, player.buddyId)}
                  />
                </Table.Cell>
                <Table.Cell>{player.rank}</Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="3">
              <Button
                floated="right"
                icon
                labelPosition="left"
                primary
                size="small"
              >
                <Icon name="cog" /> Scoring options
              </Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  }
}
