import React from 'react';
import MatchCardContent from './MatchCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import { Container, Header } from 'semantic-ui-react';

const Matches = (props) => { 
    return ( 
        <Container>
            <Header as='h3' block>
                Last played matches
            </Header>
            { props.matches ?
                <ItemCards 
                    items={props.matches} 
                    cardContentComponent={MatchCardContent}
                    hasMore={props.matches.length < props.matchCount}
                    onLoadMoreClick={props.onLoadMore}
                    onDeleteItemClick={props.onDeleteMatch}
                    onQueryChanged={props.onUpdateSearchQuery} 
                    onDetailsClicked={props.onDetailsClicked}
                    onNewItemClicked={props.onNewItemClicked}
                    query={props.searchQuery}
                />
                : null
            }
        </Container>
    );
}

export default Matches;
