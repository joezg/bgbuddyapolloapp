import React, { Component } from 'react';
import { Tab, Segment, Header, Message, Button } from 'semantic-ui-react';

import styles from './EditMatch.module.scss';
import BasicData from './editMatch/BasicData';
import ChooseMultipleItems from './editMatch/ChooseMultipleItems';
import ScoreTable from './editMatch/ScoreTable';

class EditMatch extends Component {
  constructor(props) {
    super(props);

    if (props.match) {
      this.setMatchToEditMatchProp(props.match);
    }

    this.state = {
      lastMatchDuplicated: false
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.match && !prevProps.match) {
      this.setMatchToEditMatchProp(this.props.match);
    }
  }

  render() {
    const panes = [
      {
        menuItem: 'General',
        render: () => (
          <Tab.Pane attached={false}>
            <BasicData
              {...this.props}
              onGameSelected={this.handleGameSelected}
              onLocationSelected={this.handleLocationSelected}
              onDateChange={this.handleDateChange}
              onAddNewItem={this.props.onAddNewItem}
            />
          </Tab.Pane>
        )
      },
      {
        menuItem: 'Players',
        render: () => (
          <Tab.Pane attached={false}>
            <ChooseMultipleItems
              onItemSelected={this.handleBuddySelected}
              onItemRemove={this.handleBuddyRemove}
              onCustomAddClick={this.handleAnonymousAdd}
              customAddLabel="Add anonymous"
              items={this.props.buddies}
              itemsCount={this.props.buddyCount}
              picked={this.props.currentEditMatch.players}
              icon="users"
              itemNamePlural="players"
              searchQuery={this.props.searchQueries.buddies}
              onLoadMoreClick={this.props.loadMoreBuddies}
              onQueryChanged={query =>
                this.props.onUpdateSearchQueries({ buddies: query })
              }
              pickedIdField="buddyId"
              onAddNewItem={() => this.props.onAddNewItem('buddies')}
            />
          </Tab.Pane>
        )
      },
      {
        menuItem: 'Scores',
        render: () => (
          <Tab.Pane attached={false}>
            <ScoreTable
              players={this.props.currentEditMatch.players}
              onScoreChanged={this.handleScoreChanged}
            />
          </Tab.Pane>
        )
      }
    ];

    let duplicateMatchLabel = '';
    if (this.props.lastMatch) {
      duplicateMatchLabel = this.props.lastMatch.game;
      this.props.lastMatch.location &&
        (duplicateMatchLabel += ` @${this.props.lastMatch.location}`);
      this.props.lastMatch.players.length > 0 &&
        (duplicateMatchLabel += ` with ${this.props.lastMatch.players
          .map(p => p.name)
          .join(', ')}`);
    }

    return (
      <Segment>
        <Header block>New match</Header>
        {this.props.lastMatch && !this.state.lastMatchDuplicated && (
          <Message color="teal">
            <Message.Header>Duplicate last match?</Message.Header>
            <p>{duplicateMatchLabel}</p>
            <Button primary compact onClick={this.handleLastMatchFetchClick}>
              Do it!
            </Button>
          </Message>
        )}

        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />

        <Button
          disabled={!this.props.currentEditMatch.game}
          className={styles.editMatchSave}
          primary
          onClick={this.handleSaveClick}
        >
          Save
        </Button>
      </Segment>
    );
  }

  handleDateChange = date => {
    this.props.onCurrentEditMatchChanged({ date: date.toUTCString() });
  };

  setMatchToEditMatchProp = match => {
    let game = null;
    if (match.gameId) {
      game = {
        _id: match.gameId,
        name: match.game
      };
    }

    let location = null;
    if (match.locationId) {
      location = {
        _id: match.locationId,
        name: match.location
      };
    }

    let players = [];
    if (match.players && match.players.length > 0) {
      players = match.players.map(player => {
        return {
          _id: player._id,
          buddyId: player.buddyId ? player.buddyId : null,
          name: player.name ? player.name : 'Anonymous',
          isAnonymous: !!player.isAnonymous,
          result: player.result === undefined ? 0 : player.result,
          rank: player.rank === undefined ? 0 : player.rank,
          winner: player.winner === undefined ? 0 : player.winner
        };
      });
    }

    this.props.onCurrentEditMatchChanged({
      game,
      location,
      players
    });
  };

  handleLastMatchFetchClick = () => {
    const match = this.props.lastMatch;

    this.setMatchToEditMatchProp(match);

    this.setState({
      lastMatchDuplicated: true
    });
  };

  handleScoreChanged = players => {
    this.props.onCurrentEditMatchChanged({ players });
  };

  handleGameSelected = game => {
    this.props.onCurrentEditMatchChanged({ game });
  };

  handleLocationSelected = location => {
    this.props.onCurrentEditMatchChanged({ location });
  };

  handleAnonymousAdd = () => {
    this.addPlayer({
      isAnonymous: true,
      buddyId: null,
      name: 'Anonymous'
    });
  };

  addPlayer = player => {
    player.result = 0;
    player.rank = null;
    player.winner = false;

    if (
      player.isAnonymous ||
      !this.props.currentEditMatch.players.find(p => {
        return p.buddyId === player.buddyId;
      })
    ) {
      this.props.onCurrentEditMatchChanged({
        players: [...this.props.currentEditMatch.players, player]
      });
    }
  };

  handleBuddySelected = buddy => {
    this.addPlayer({
      name: buddy.name,
      buddyId: buddy._id
    });
  };

  handleBuddyRemove = buddy => {
    this.props.onCurrentEditMatchChanged({
      players: this.props.currentEditMatch.players.filter(
        player => player.buddyId !== buddy.buddyId
      )
    });
  };

  handleSaveClick = () => {
    const newMatch = this.props.currentEditMatch;
    const saveMatchVariables = {
      gameId: newMatch.game._id,
      locationId: newMatch.location ? newMatch.location._id : null,
      players: newMatch.players.map(player => ({
        _id: player._id,
        buddyId: player.buddyId,
        rank: player.rank,
        result: player.result,
        winner: player.winner,
        isAnonymous: !!player.isAnonymous
      })),
      date: {
        raw: newMatch.date
      },
      isScored: true
    };

    this.props.submitMatch(saveMatchVariables);
  };
}

export default EditMatch;
