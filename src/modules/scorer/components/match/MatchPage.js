import React, { Component } from 'react';
import { Segment, Header, Label } from 'semantic-ui-react';
import ItemCards from '../../../../components/common/ItemCards';
import PlayerCardContent from './PlayerCardContent';
import styles from './MatchPage.module.scss';
import FloatingButton from '../../../../components/common/FloatingButton';

class MatchPage extends Component {
  render() {
    const match = this.props.match;
    const subheader = match
      ? `${match.date.relative} at ${match.location}`
      : '...';
    const ribbonVissible = match && (match.isNewGame || match.isGameOutOfDust);
    return (
      <div>
        <Header
          size="large"
          content={match ? match.game : '...'}
          subheader={subheader}
          block
        />
        {match ? (
          <Segment raised>
            {match.isNewGame ? (
              <Label className={styles.ribbon} color="orange" ribbon="right">
                New game
              </Label>
            ) : null}
            {match.isGameOutOfDust ? (
              <Label className={styles.ribbon} color="brown" ribbon="right">
                Out of dust
              </Label>
            ) : null}
            <Header
              size="medium"
              content="Players"
              style={{
                marginTop: ribbonVissible ? '-33px' : '0'
              }}
              block
            />
            <Segment>
              <ItemCards
                items={match.players}
                cardContentComponent={PlayerCardContent}
                context={match}
                hideCardActions={true}
                hideSearch={true}
              />
            </Segment>
          </Segment>
        ) : null}
        <FloatingButton
          onClick={this.props.onEditItemClicked}
          iconName="edit"
        />
      </div>
    );
  }
}

export default MatchPage;
