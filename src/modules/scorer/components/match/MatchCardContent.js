import React, { Component } from 'react';
import { Card, List, Popup, Label } from 'semantic-ui-react';
import styles from './MatchCardContent.module.scss';

class MatchCardContent extends Component {
  render() {
    const item = this.props.item;
    const subtitle = item.date.relative + (item.location ? ' at ' + item.location : ''); 
    
    return (
      <Card.Content>
        <Card.Header>
          { item.isNewGame ? <Label className={styles.info} color='orange' tag>New game</Label> : null }
          { item.isGameOutOfDust ? <Label className={styles.info} color='brown' tag>Out of dust</Label> : null }
          {item.game}
        </Card.Header>
        <Card.Meta>
          {subtitle}
        </Card.Meta>
        <Card.Description>
          <List>
            { item.players.map((player)=>{
              let nameComponent = player.isAnonymous ?
                  <List.Content as="em" content="Anonymous" /> :
                  <List.Content content={player.name} />;
              
              return (
                <List.Item key={player._id}>
                    <List.Icon name='user' />
                    { nameComponent }
                    { player.winner ? 
                        <Popup
                          trigger={<List.Icon name='trophy' />}
                          position="right center"
                          content="winner"
                        /> : null }
                    
                </List.Item>
              )
            })}
          </List>
        </Card.Description>
      </Card.Content>
    );
  }
}

export default MatchCardContent;

