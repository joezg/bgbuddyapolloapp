import React, { Component } from 'react';
import { Card, Label, Popup } from 'semantic-ui-react';
import styles from './PlayerCardContent.module.scss';

class PlayerCardContent extends Component {
  render() {
    const player = this.props.item;
    return (
      <Card.Content>
        <Card.Header content={player.isAnonymous ? <em>"Anonymous"</em> : player.name} />
        <Card.Description>
          { player.winner ? 
            <Popup
                trigger={<Label icon="trophy" corner="right" />}
                position="top center"
                content="winner"
            /> : null }
          { this.props.context.isScored ? 
            <div>
              <p>
                Rank: {player.rank}
              </p>
              <p>
                Result: {player.result}
              </p>
            </div> : <p className={styles.playerScoreNotScored}>Match is not scored</p> }
        </Card.Description>
      </Card.Content>
    );
  }
}

export default PlayerCardContent;