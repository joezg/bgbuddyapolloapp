import React, { Component } from 'react';
import { Segment, Header, Icon, Popup } from 'semantic-ui-react';
import MatchCardContent from '../match/MatchCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import FloatingButton from '../../../../components/common/FloatingButton';

class BuddyPage extends Component {
  render() {
    const buddy = this.props.buddy;
    return (
      <div>
        <Header size="large" block>
          {buddy && buddy.like ? (
            <Popup trigger={<Icon size="small" name="like outline" />}>
              Favourite Buddy
            </Popup>
          ) : null}
          <Header.Content>{buddy ? buddy.name : '...'}</Header.Content>
        </Header>
        {buddy ? (
          <Segment raised>
            <p>
              You played <strong>{buddy.matchCount} matches </strong> together.
            </p>
            <Header size="medium" content="Matches" block />
            <Segment>
              <ItemCards
                items={buddy.matches}
                cardContentComponent={MatchCardContent}
                onDeleteItemClick={this.props.onDeleteMatch}
                hideSearch={true}
                detailsPath="/match/"
              />
            </Segment>
            <FloatingButton
              onClick={this.props.onEditItemClicked}
              iconName="edit"
            />
          </Segment>
        ) : null}
      </div>
    );
  }
}

export default BuddyPage;
