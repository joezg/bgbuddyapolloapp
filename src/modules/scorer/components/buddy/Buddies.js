import React from 'react';
import BuddyCardContent from './BuddyCardContent';
import ItemCards from '../../../../components/common/ItemCards';
import { Container, Header } from 'semantic-ui-react';

export default (props) => {
    return ( 
        <Container>
            <Header as='h3' block>
                My buddies
            </Header>
            { props.buddies ? 
                <ItemCards 
                    items={props.buddies} 
                    cardContentComponent={BuddyCardContent}
                    hasMore={props.buddies.length < props.buddyCount}
                    onLoadMoreClick={props.onLoadMore}
                    onDeleteItemClick={props.onDeleteBuddy}
                    onQueryChanged={props.onUpdateSearchQuery} 
                    query={props.searchQuery}
                    onDetailsClicked={props.onDetailsClicked}
                    canDelete={(item)=>item.matchCount === 0}
                    onNewItemClicked={props.onNewItemClicked}
                />
                : null }
        </Container>
    );
}
