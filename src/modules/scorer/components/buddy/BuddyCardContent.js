import React, { Component } from 'react';
import { Card, Icon, Popup } from 'semantic-ui-react';
import styles from './BuddyCardContent.module.scss';

class BuddyCardContent extends Component {
  render() {
    const item = this.props.item;
    
    const playedNumberTimes = <span>played <strong>{item.matchCount}</strong> {item.matchCount === 1 ? ' time' : ' times' }</span>;
    const numberOfWins = item.winCount || item.winCount === 0 ? <span>won <strong>{item.winCount}</strong> {item.winCount === 1 ? ' time' : ' times' }</span> : null;
    
    return (
      <Card.Content>
        <Card.Header>
          {item.name}
          {item.like ? 
            <Popup trigger={<Icon className={styles.likeIcon} circular inverted color="red" size="small" floated="right" name='like outline' />} >
              Favourite Buddy
            </Popup>
          : null}
        </Card.Header>
        <Card.Meta>
          {playedNumberTimes}
          {numberOfWins ? <br /> : null}
          {numberOfWins ? numberOfWins : null}
        </Card.Meta>
        <Card.Description>
          
        </Card.Description>
      </Card.Content>
    );
  }
}

export default BuddyCardContent;
