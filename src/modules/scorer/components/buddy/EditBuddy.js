import React from 'react';
import ActionForm from '../../../../components/common/ActionForm';

const EditBuddy = props => {
  return (
    <ActionForm
      item={props.buddy}
      fields={[
        {
          type: 'id',
          name: '_id'
        },
        {
          type: 'input',
          name: 'name',
          placeholder: 'Enter buddy name',
          label: 'Enter buddy name'
        },
        {
          type: 'switch',
          name: 'isUser',
          label: 'This is me?'
        },
        {
          type: 'switch',
          name: 'like',
          label: 'Favourite buddy?'
        }
      ]}
      formHeader={props.buddy ? `Edit "${props.buddy.name}"` : 'New Buddy'}
      onSubmit={props.onSubmitBuddy}
    />
  );
};

export default EditBuddy;
