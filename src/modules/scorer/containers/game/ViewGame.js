import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { GamePage } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDeleteItemMatch } from '../../../../helpers/mutation.helpers';

const DELETE_MATCH_MUTATION = gql`
  mutation DeleteMatch($_id: ID!) {
    deleteMatch(_id: $_id) {
      _id
    }
  }
`;

const GET_GAME_DETAILS_QUERY = gql`
  query GetGameDetails($id: ID!) {
    viewer {
      _id
      game(_id: $id) {
        _id
        name
        owned
        wantToPlay
        matchCount
        previousPlaysNumber
        matches {
          _id
          date {
            relative
          }
          game
          isNewGame
          isGameOutOfDust
          location
          players {
            _id
            winner
            name
            isAnonymous
          }
        }
      }
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_MATCH_MUTATION}
      update={updateAfterDeleteItemMatch({
        query: GET_GAME_DETAILS_QUERY,
        id: props.id
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteMatch = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={GET_GAME_DETAILS_QUERY}
              variables={{ id: props.id }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                const handleEditItemClicked = () => {
                  props.onEditItemClicked(data.viewer.game._id);
                };
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <GamePage
                      onEditItemClicked={handleEditItemClicked}
                      onDeleteMatch={handleDeleteMatch}
                      game={data && data.viewer && data.viewer.game}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
