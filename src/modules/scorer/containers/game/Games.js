import React from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Games } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDelete } from '../../../../helpers/mutation.helpers';
import { loadMoreHandler } from '../../../../helpers/query.helpers';

const GAMES_COLLECTION_QUERY = gql`
  query GetGamesCollection($first: Int!, $after: Int!, $text: String) {
    viewer {
      _id
      games(first: $first, after: $after, text: $text)
        @connection(key: "games", filter: ["text"]) {
        _id
        name
        owned
        played
        wantToPlay
        dateFirstPlayed {
          relative
        }
        dateLastPlayed {
          relative
        }
        matchCount
        previousPlaysNumber
      }
      gameCount(text: $text)
    }
  }
`;

const DELETE_GAME_MUTATION = gql`
  mutation DeleteGameMutation($_id: ID!) {
    deleteGame(_id: $_id) {
      _id
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_GAME_MUTATION}
      update={updateAfterDelete({
        query: GAMES_COLLECTION_QUERY,
        searchQuery: props.searchQuery
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteGame = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={GAMES_COLLECTION_QUERY}
              variables={{ first: 12, after: 0, text: props.searchQuery }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = loadMoreHandler({ data, fetchMore });
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <Games
                      onLoadMore={handleLoadMore}
                      games={data && data.viewer ? data.viewer.games : []}
                      gameCount={
                        data && data.viewer ? data.viewer.gameCount : 0
                      }
                      onUpdateSearchQuery={props.onUpdateSearchQuery}
                      searchQuery={props.searchQuery}
                      onDeleteGame={handleDeleteGame}
                      onDetailsClicked={props.onDetailsClicked}
                      onNewItemClicked={props.onNewItemClicked}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
