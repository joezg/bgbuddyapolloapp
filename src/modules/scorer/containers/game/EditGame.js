import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query, ApolloConsumer } from 'react-apollo';
import { EditGame } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';

const EDIT_GAME_MUTATION = gql`
  mutation EditGame(
    $_id: ID!
    $name: String!
    $owned: Boolean
    $played: Boolean
    $wantToPlay: Boolean
    $dateFirstPlayed: String
    $dateLastPlayed: String
    $previousPlaysNumber: Int
    $bggId: Int
  ) {
    editGame(
      _id: $_id
      name: $name
      owned: $owned
      played: $played
      wantToPlay: $wantToPlay
      dateFirstPlayed: $dateFirstPlayed
      dateLastPlayed: $dateLastPlayed
      previousPlaysNumber: $previousPlaysNumber
      bggId: $bggId
    ) {
      _id
      name
      owned
      wantToPlay
      matchCount
      previousPlaysNumber
      played
      bggId
    }
  }
`;

const GET_GAME_QUERY = gql`
  query GetGame($id: ID!) {
    viewer {
      _id
      game(_id: $id) {
        _id
        name
        owned
        wantToPlay
        matchCount
        previousPlaysNumber
        played
        bggId
      }
    }
  }
`;

export const BGG_SEARCH_QUERY = gql`
  query BggSearch($queryString: String!) {
    bgg {
      search(query: $queryString) {
        id
        name
        year
        thumbnail
      }
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={EDIT_GAME_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          await submitItem({ variables: item });
          props.onSubmitComplete();
        };

        return (
          <>
            <ApolloConsumer>
              {client => {
                const handleBggSearch = async queryString => {
                  const result = await client.query({
                    query: BGG_SEARCH_QUERY,
                    variables: { queryString }
                  });
                  if (
                    !result.data ||
                    !result.data.bgg ||
                    !result.data.bgg.search
                  ) {
                    return [];
                  }

                  return result.data.bgg.search;
                };
                return (
                  <Query
                    query={GET_GAME_QUERY}
                    variables={{ id: props.id }}
                    fetchPolicy="cache-and-network"
                  >
                    {({ loading, error, data }) => {
                      return (
                        <ResponseHandler loading={loading} error={error}>
                          <EditGame
                            onSubmitGame={handleSubmit}
                            onBggSearch={handleBggSearch}
                            game={data && data.viewer ? data.viewer.game : {}}
                          />
                        </ResponseHandler>
                      );
                    }}
                  </Query>
                );
              }}
            </ApolloConsumer>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
