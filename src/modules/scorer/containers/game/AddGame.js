import gql from 'graphql-tag';
import React from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import { EditGame } from '../../components';
import { Loader } from '../../../../components';
import { BGG_SEARCH_QUERY } from './EditGame';

const ADD_GAME_MUTATION = gql`
  mutation AddGame(
    $name: String!
    $owned: Boolean
    $played: Boolean
    $wantToPlay: Boolean
    $dateFirstPlayed: String
    $dateLastPlayed: String
    $previousPlaysNumber: Int
    $bggId: Int
  ) {
    addGame(
      name: $name
      owned: $owned
      played: $played
      wantToPlay: $wantToPlay
      dateFirstPlayed: $dateFirstPlayed
      dateLastPlayed: $dateLastPlayed
      previousPlaysNumber: $previousPlaysNumber
      bggId: $bggId
    ) {
      _id
      name
      owned
      wantToPlay
      matchCount
      previousPlaysNumber
      played
      bggId
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={ADD_GAME_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          const result = await submitItem({ variables: item });
          props.onSubmitComplete({ game: result.data.addGame });
        };

        return (
          <>
            <ApolloConsumer>
              {client => {
                const handleBggSearch = async queryString => {
                  const result = await client.query({
                    query: BGG_SEARCH_QUERY,
                    variables: { queryString }
                  });
                  if (
                    !result.data ||
                    !result.data.bgg ||
                    !result.data.bgg.search
                  ) {
                    return [];
                  }

                  return result.data.bgg.search;
                };
                return (
                  <EditGame
                    onSubmitGame={handleSubmit}
                    onBggSearch={handleBggSearch}
                  />
                );
              }}
            </ApolloConsumer>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
