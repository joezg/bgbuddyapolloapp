import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { LocationPage } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDeleteItemMatch } from '../../../../helpers/mutation.helpers';

const DELETE_MATCH_MUTATION = gql`
  mutation DeleteMatch($_id: ID!) {
    deleteMatch(_id: $_id) {
      _id
    }
  }
`;

const GET_LOCATION_DETAILS_QUERY = gql`
  query GetLocationDetails($id: ID!) {
    viewer {
      _id
      location(_id: $id) {
        _id
        name
        buddyCount
        matchCount
        buddies {
          _id
          name
          matchCount
          winCount
        }
        matches {
          _id
          date {
            relative
          }
          game
          isNewGame
          isGameOutOfDust
          location
          players {
            _id
            winner
            name
            isAnonymous
          }
        }
      }
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_MATCH_MUTATION}
      update={updateAfterDeleteItemMatch({
        query: GET_LOCATION_DETAILS_QUERY,
        id: props.id
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteMatch = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={GET_LOCATION_DETAILS_QUERY}
              variables={{ id: props.id }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                const handleEditItemClicked = () => {
                  props.onEditItemClicked(data.viewer.location._id);
                };
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <LocationPage
                      onEditItemClicked={handleEditItemClicked}
                      onDeleteMatch={handleDeleteMatch}
                      location={data && data.viewer && data.viewer.location}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
