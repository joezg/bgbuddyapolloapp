import gql from 'graphql-tag';
import React from 'react';
import { Mutation } from 'react-apollo';
import { EditLocation } from '../../components';
import { Loader } from '../../../../components';

const ADD_LOCATION_MUTATION = gql`
  mutation AddLocation($name: String!) {
    addLocation(name: $name) {
      _id
      name
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={ADD_LOCATION_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          const result = await submitItem({ variables: item });
          props.onSubmitComplete({ location: result.data.addLocation });
        };

        return (
          <>
            <EditLocation onSubmitLocation={handleSubmit} />
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
