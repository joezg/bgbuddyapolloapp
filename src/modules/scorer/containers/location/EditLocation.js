import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { EditLocation } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';

const EDIT_LOCATION_MUTATION = gql`
  mutation EditLocation($_id: ID!, $name: String!) {
    editLocation(_id: $_id, name: $name) {
      _id
      name
    }
  }
`;

const GET_LOCATION_QUERY = gql`
  query GetLocation($id: ID!) {
    viewer {
      _id
      location(_id: $id) {
        _id
        name
      }
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={EDIT_LOCATION_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          await submitItem({ variables: item });
          props.onSubmitComplete();
        };

        return (
          <>
            <Query
              query={GET_LOCATION_QUERY}
              variables={{ id: props.id }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <EditLocation
                      onSubmitLocation={handleSubmit}
                      location={data && data.viewer ? data.viewer.location : {}}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
