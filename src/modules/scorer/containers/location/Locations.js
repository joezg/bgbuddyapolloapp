import React from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Locations } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDelete } from '../../../../helpers/mutation.helpers';
import { loadMoreHandler } from '../../../../helpers/query.helpers';

const LOCATIONS_COLLECTION_QUERY = gql`
  query GetLocationsCollection($first: Int!, $after: Int!, $text: String) {
    viewer {
      _id
      locations(first: $first, after: $after, text: $text)
        @connection(key: "locations", filter: ["text"]) {
        _id
        name
        matchCount
        buddyCount
      }
      locationCount(text: $text)
    }
  }
`;

const DELETE_LOCATION_MUTATION = gql`
  mutation DeleteGruopMutation($_id: ID!) {
    deleteLocation(_id: $_id) {
      _id
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_LOCATION_MUTATION}
      update={updateAfterDelete({
        query: LOCATIONS_COLLECTION_QUERY,
        searchQuery: props.searchQuery
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteLocation = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={LOCATIONS_COLLECTION_QUERY}
              variables={{ first: 12, after: 0, text: props.searchQuery }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = loadMoreHandler({ data, fetchMore });
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <Locations
                      onLoadMore={handleLoadMore}
                      locations={
                        data && data.viewer ? data.viewer.locations : []
                      }
                      locationCount={
                        data && data.viewer ? data.viewer.locationCount : 0
                      }
                      onUpdateSearchQuery={props.onUpdateSearchQuery}
                      searchQuery={props.searchQuery}
                      onDeleteLocation={handleDeleteLocation}
                      onDetailsClicked={props.onDetailsClicked}
                      onNewItemClicked={props.onNewItemClicked}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
