import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { EditMatch } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';

const ADD_MATCH_MUTATION = gql`
  mutation AddMatch(
    $gameId: ID!
    $locationId: ID
    $players: [PlayerInput]!
    $date: DateInput!
    $isScored: Boolean!
  ) {
    addMatch(
      gameId: $gameId
      locationId: $locationId
      players: $players
      date: $date
      isScored: $isScored
    ) {
      _id
      gameId
      locationId
    }
  }
`;

const GET_ALL_COLLECTIONS_AND_LAST_MATCH_QUERY = gql`
  query getAllCollections(
    $gamesFirst: Int!
    $gamesAfter: Int!
    $gamesText: String
    $buddiesFirst: Int!
    $buddiesAfter: Int!
    $buddiesText: String
    $locationsFirst: Int!
    $locationsAfter: Int!
    $locationsText: String
  ) {
    viewer {
      _id
      games(first: $gamesFirst, after: $gamesAfter, text: $gamesText) {
        _id
        name
      }
      buddies(first: $buddiesFirst, after: $buddiesAfter, text: $buddiesText) {
        _id
        name
      }
      locations(
        first: $locationsFirst
        after: $locationsAfter
        text: $locationsText
      ) {
        _id
        name
      }
      gameCount(text: $gamesText)
      buddyCount(text: $buddiesText)
      locationCount(text: $locationsText)
      matches(first: 1, after: 0) {
        _id
        gameId
        game
        locationId
        location
        players {
          name
          buddyId
          isUser
          isAnonymous
        }
      }
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={ADD_MATCH_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          await submitItem({ variables: item });
          props.onSubmitComplete();
        };

        return (
          <>
            <Query
              query={GET_ALL_COLLECTIONS_AND_LAST_MATCH_QUERY}
              variables={{
                gamesFirst: 12,
                gamesAfter: 0,
                gamesText: props.searchQueries.games,
                buddiesFirst: 12,
                buddiesAfter: 0,
                buddiesText: props.searchQueries.buddies,
                locationsFirst: 12,
                locationsAfter: 0,
                locationsText: props.searchQueries.locations
              }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = collection => {
                  return numberOfItems => {
                    const fetchMoreVariables = {
                      [`${collection}After`]: data.viewer[collection].length
                    };
                    if (numberOfItems) {
                      fetchMoreVariables.first = numberOfItems;
                    }

                    fetchMore({
                      variables: fetchMoreVariables,
                      updateQuery(previousResult, { fetchMoreResult }) {
                        return {
                          ...previousResult,
                          viewer: {
                            ...previousResult.viewer,
                            [collection]: [
                              ...previousResult.viewer[collection],
                              ...fetchMoreResult.viewer[collection]
                            ]
                          }
                        };
                      }
                    });
                  };
                };

                const {
                  games = [],
                  locations = [],
                  buddies = [],
                  gameCount = 0,
                  locationCount = 0,
                  buddyCount = 0,
                  matches = []
                } = data && data.viewer ? data.viewer : {};

                return (
                  <ResponseHandler loading={loading} error={error}>
                    <EditMatch
                      onSubmitMatch={handleSubmit}
                      currentEditMatch={props.currentEditMatch}
                      searchQueries={props.searchQueries}
                      games={games}
                      locations={locations}
                      buddies={buddies}
                      gameCount={gameCount}
                      locationCount={locationCount}
                      buddyCount={buddyCount}
                      onUpdateSearchQueries={props.onUpdateSearchQueries}
                      onCurrentEditMatchChanged={
                        props.onCurrentEditMatchChanged
                      }
                      loadMoreGames={handleLoadMore('games')}
                      loadMoreLocations={handleLoadMore('locations')}
                      loadMoreBuddies={handleLoadMore('buddies')}
                      lastMatch={matches.length > 0 ? matches[0] : null}
                      submitMatch={handleSubmit}
                      onAddNewItem={props.onAddNewItem}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>

            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
