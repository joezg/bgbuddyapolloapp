import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { EditMatch } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';

const EDIT_MATCH_MUTATION = gql`
  mutation EditMatch(
    $id: ID!
    $gameId: ID!
    $locationId: ID
    $players: [PlayerInput]!
    $date: DateInput!
    $isScored: Boolean!
  ) {
    editMatch(
      _id: $id
      gameId: $gameId
      locationId: $locationId
      players: $players
      date: $date
      isScored: $isScored
    ) {
      _id
      gameId
      locationId
    }
  }
`;

const GET_ALL_COLLECTIONS_AND_CURRENT_MATCH_QUERY = gql`
  query getAllCollections(
    $gamesFirst: Int!
    $gamesAfter: Int!
    $gamesText: String
    $buddiesFirst: Int!
    $buddiesAfter: Int!
    $buddiesText: String
    $locationsFirst: Int!
    $locationsAfter: Int!
    $locationsText: String
    $matchId: ID!
  ) {
    viewer {
      _id
      games(first: $gamesFirst, after: $gamesAfter, text: $gamesText) {
        _id
        name
      }
      buddies(first: $buddiesFirst, after: $buddiesAfter, text: $buddiesText) {
        _id
        name
      }
      locations(
        first: $locationsFirst
        after: $locationsAfter
        text: $locationsText
      ) {
        _id
        name
      }
      gameCount(text: $gamesText)
      buddyCount(text: $buddiesText)
      locationCount(text: $locationsText)
      match(_id: $matchId) {
        _id
        gameId
        game
        locationId
        location
        players {
          _id
          name
          buddyId
          isUser
          isAnonymous
          rank
          result
          winner
        }
      }
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={EDIT_MATCH_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          item.id = props.id;
          await submitItem({ variables: item });
          props.onSubmitComplete();
        };

        return (
          <>
            <Query
              query={GET_ALL_COLLECTIONS_AND_CURRENT_MATCH_QUERY}
              variables={{
                matchId: props.id,
                gamesFirst: 12,
                gamesAfter: 0,
                gamesText: props.searchQueries.games,
                buddiesFirst: 12,
                buddiesAfter: 0,
                buddiesText: props.searchQueries.buddies,
                locationsFirst: 12,
                locationsAfter: 0,
                locationsText: props.searchQueries.locations
              }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = collection => {
                  return numberOfItems => {
                    const fetchMoreVariables = {
                      [`${collection}After`]: data.viewer[collection].length
                    };
                    if (numberOfItems) {
                      fetchMoreVariables.first = numberOfItems;
                    }

                    fetchMore({
                      variables: fetchMoreVariables,
                      updateQuery(previousResult, { fetchMoreResult }) {
                        return {
                          ...previousResult,
                          viewer: {
                            ...previousResult.viewer,
                            [collection]: [
                              ...previousResult.viewer[collection],
                              ...fetchMoreResult.viewer[collection]
                            ]
                          }
                        };
                      }
                    });
                  };
                };

                const {
                  games = [],
                  locations = [],
                  buddies = [],
                  gameCount = 0,
                  locationCount = 0,
                  buddyCount = 0,
                  match = null
                } = data && data.viewer ? data.viewer : {};

                return (
                  <ResponseHandler loading={loading} error={error}>
                    <EditMatch
                      onSubmitMatch={handleSubmit}
                      currentEditMatch={props.currentEditMatch}
                      searchQueries={props.searchQueries}
                      games={games}
                      locations={locations}
                      buddies={buddies}
                      gameCount={gameCount}
                      locationCount={locationCount}
                      buddyCount={buddyCount}
                      onUpdateSearchQueries={props.onUpdateSearchQueries}
                      onCurrentEditMatchChanged={
                        props.onCurrentEditMatchChanged
                      }
                      loadMoreGames={handleLoadMore('games')}
                      loadMoreLocations={handleLoadMore('locations')}
                      loadMoreBuddies={handleLoadMore('buddies')}
                      match={match}
                      submitMatch={handleSubmit}
                      onAddNewItem={props.onAddNewItem}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>

            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
