import React from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Matches } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDelete } from '../../../../helpers/mutation.helpers';
import { loadMoreHandler } from '../../../../helpers/query.helpers';

const MATCHES_COLLECTION_QUERY = gql`
  query GetMatchesCollection($first: Int!, $after: Int!, $text: String) {
    viewer {
      _id
      matches(first: $first, after: $after, text: $text)
        @connection(key: "matches", filter: ["text"]) {
        _id
        date {
          relative
          raw
        }
        game
        isNewGame
        isGameOutOfDust
        location
        players {
          _id
          winner
          name
          isAnonymous
        }
      }
      matchCount(text: $text)
    }
  }
`;

const DELETE_MATCH_MUTATION = gql`
  mutation DeleteMatchMutation($_id: ID!) {
    deleteMatch(_id: $_id) {
      _id
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_MATCH_MUTATION}
      update={updateAfterDelete({
        query: MATCHES_COLLECTION_QUERY,
        searchQuery: props.searchQuery
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteMatch = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={MATCHES_COLLECTION_QUERY}
              variables={{ first: 12, after: 0, text: props.searchQuery }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = loadMoreHandler({ data, fetchMore });
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <Matches
                      onLoadMore={handleLoadMore}
                      matches={data && data.viewer ? data.viewer.matches : []}
                      matchCount={
                        data && data.viewer ? data.viewer.matchCount : 0
                      }
                      onUpdateSearchQuery={props.onUpdateSearchQuery}
                      searchQuery={props.searchQuery}
                      onDeleteMatch={handleDeleteMatch}
                      onDetailsClicked={props.onDetailsClicked}
                      onNewItemClicked={props.onNewItemClicked}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
