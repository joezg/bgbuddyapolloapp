import gql from 'graphql-tag';
import React from 'react';
import { Query } from 'react-apollo';
import { MatchPage } from '../../components';
import { ResponseHandler } from '../../../../components';

const GET_MATCH_DETAILS_QUERY = gql`
  query GetGroupDetails($id: ID!) {
    viewer {
      _id
      match(_id: $id) {
        _id
        game
        date {
          relative
        }
        location
        isNewGame
        isGameOutOfDust
        isScored
        players {
          _id
          winner
          result
          rank
          name
          name
          isAnonymous
        }
      }
    }
  }
`;

export default props => {
  return (
    <Query
      query={GET_MATCH_DETAILS_QUERY}
      variables={{ id: props.id }}
      fetchPolicy="cache-and-network"
    >
      {({ loading, error, data }) => {
        const handleEditItemClicked = () => {
          props.onEditItemClicked(data.viewer.match._id);
        };
        return (
          <ResponseHandler loading={loading} error={error}>
            <MatchPage
              onEditItemClicked={handleEditItemClicked}
              match={data && data.viewer && data.viewer.match}
            />
          </ResponseHandler>
        );
      }}
    </Query>
  );
};
