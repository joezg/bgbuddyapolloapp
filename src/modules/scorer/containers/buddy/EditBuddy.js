import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { EditBuddy } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';

const EDIT_BUDDY_MUTATION = gql`
  mutation EditBuddy(
    $_id: ID!
    $name: String!
    $like: Boolean!
    $isUser: Boolean!
  ) {
    editBuddy(_id: $_id, name: $name, like: $like, isUser: $isUser) {
      _id
      name
      like
      isUser
    }
  }
`;

const GET_BUDDY_QUERY = gql`
  query GetBuddy($id: ID!) {
    viewer {
      _id
      buddy(_id: $id) {
        _id
        name
        like
        isUser
      }
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={EDIT_BUDDY_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          await submitItem({ variables: item });
          props.onSubmitComplete();
        };

        return (
          <>
            <Query
              query={GET_BUDDY_QUERY}
              variables={{ id: props.id }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <EditBuddy
                      onSubmitBuddy={handleSubmit}
                      buddy={data && data.viewer ? data.viewer.buddy : {}}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
