import React from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Buddies } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDelete } from '../../../../helpers/mutation.helpers';
import { loadMoreHandler } from '../../../../helpers/query.helpers';

const BUDDIES_COLLECTION_QUERY = gql`
  query GetBuddiesCollection($first: Int!, $after: Int!, $text: String) {
    viewer {
      _id
      buddies(first: $first, after: $after, text: $text)
        @connection(key: "buddies", filter: ["text"]) {
        _id
        name
        like
        isUser
        matchCount
      }
      buddyCount(text: $text)
    }
  }
`;

const DELETE_BUDDY_MUTATION = gql`
  mutation DeleteBuddyMutation($_id: ID!) {
    deleteBuddy(_id: $_id) {
      _id
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_BUDDY_MUTATION}
      update={updateAfterDelete({
        query: BUDDIES_COLLECTION_QUERY,
        searchQuery: props.searchQuery
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteBuddy = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={BUDDIES_COLLECTION_QUERY}
              variables={{ first: 12, after: 0, text: props.searchQuery }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data, fetchMore }) => {
                const handleLoadMore = loadMoreHandler({ data, fetchMore });
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <Buddies
                      onLoadMore={handleLoadMore}
                      buddies={data && data.viewer ? data.viewer.buddies : []}
                      buddyCount={
                        data && data.viewer ? data.viewer.buddyCount : 0
                      }
                      onUpdateSearchQuery={props.onUpdateSearchQuery}
                      searchQuery={props.searchQuery}
                      onDeleteBuddy={handleDeleteBuddy}
                      onDetailsClicked={props.onDetailsClicked}
                      onNewItemClicked={props.onNewItemClicked}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
