import gql from 'graphql-tag';
import React from 'react';
import { Mutation, Query } from 'react-apollo';
import { BuddyPage } from '../../components';
import { ResponseHandler, Loader } from '../../../../components';
import { updateAfterDeleteItemMatch } from '../../../../helpers/mutation.helpers';

const DELETE_MATCH_MUTATION = gql`
  mutation DeleteMatch($_id: ID!) {
    deleteMatch(_id: $_id) {
      _id
    }
  }
`;

const GET_BUDDY_DETAILS_QUERY = gql`
  query GetBuddyDetails($id: ID!) {
    viewer {
      _id
      buddy(_id: $id) {
        _id
        name
        like
        matchCount
        matches {
          _id
          date {
            relative
          }
          game
          isNewGame
          isGameOutOfDust
          location
          players {
            _id
            winner
            name
            isAnonymous
          }
        }
      }
    }
  }
`;

export default props => {
  return (
    <Mutation
      mutation={DELETE_MATCH_MUTATION}
      update={updateAfterDeleteItemMatch({
        query: GET_BUDDY_DETAILS_QUERY,
        id: props.id
      })}
    >
      {(deleteItem, { loading, error }) => {
        const handleDeleteMatch = _id => deleteItem({ variables: { _id } });

        return (
          <>
            <Query
              query={GET_BUDDY_DETAILS_QUERY}
              variables={{ id: props.id }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                const handleEditItemClicked = () => {
                  props.onEditItemClicked(data.viewer.buddy._id);
                };
                return (
                  <ResponseHandler loading={loading} error={error}>
                    <BuddyPage
                      onEditItemClicked={handleEditItemClicked}
                      onDeleteMatch={handleDeleteMatch}
                      buddy={data && data.viewer && data.viewer.buddy}
                    />
                  </ResponseHandler>
                );
              }}
            </Query>
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
