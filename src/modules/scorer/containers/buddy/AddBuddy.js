import gql from 'graphql-tag';
import React from 'react';
import { Mutation } from 'react-apollo';
import { EditBuddy } from '../../components';
import { Loader } from '../../../../components';
const ADD_BUDDY_MUTATION = gql`
  mutation AddBuddy($name: String!, $like: Boolean!, $isUser: Boolean!) {
    addBuddy(name: $name, like: $like, isUser: $isUser) {
      _id
      name
      like
      isUser
    }
  }
`;

export default props => {
  return (
    <Mutation mutation={ADD_BUDDY_MUTATION}>
      {(submitItem, { loading, error }) => {
        const handleSubmit = async item => {
          const result = await submitItem({ variables: item });
          props.onSubmitComplete({ buddy: result.data.addBuddy });
        };

        return (
          <>
            <EditBuddy onSubmitBuddy={handleSubmit} />
            <Loader key="loader" loading={loading} />
          </>
        );
      }}
    </Mutation>
  );
};
