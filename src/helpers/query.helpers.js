export const loadMoreHandler = ({ data, fetchMore }) => {
  return numberOfItems => {
    for (let prop in data.viewer) {
      if (Array.isArray(data.viewer[prop])) {
        const fetchMoreVariables = {
          after: data.viewer[prop].length
        };
        if (numberOfItems) {
          fetchMoreVariables.first = numberOfItems;
        }

        fetchMore({
          variables: fetchMoreVariables,
          updateQuery(previousResult, { fetchMoreResult }) {
            return {
              ...previousResult,
              viewer: {
                ...previousResult.viewer,
                ...fetchMoreResult.viewer,
                [prop]: [
                  ...previousResult.viewer[prop],
                  ...fetchMoreResult.viewer[prop]
                ]
              }
            };
          }
        });
      }
    }
  };
};
