import { isObject } from "./general.helpers";

export const updateAfterDelete = ({ query, searchQuery}) => {
    return (cache, result) => {
        const mutationKey = Object.keys(result.data)[0];
        const deletedItem = result.data[mutationKey];

        const options = { query, variables: {first: 1, after: 0, text: searchQuery}};
    
        const data = cache.readQuery(options);

        let collectionProp = null, countProp = null;
        for (let prop in data.viewer){
            if (Array.isArray(data.viewer[prop])){
                collectionProp = prop;
            }
            if (prop.toLowerCase().endsWith("count")){
                countProp = prop;
            }
        }
        
        let newItems = data.viewer[collectionProp].filter(item => item._id !== deletedItem._id);
                
        cache.writeQuery({
            ...options,
            data:{
                viewer:{
                    ...data.viewer,
                    [countProp]: data.viewer[countProp] - 1,
                    [collectionProp]: newItems
                }
            }
        });
    }
}

export const updateAfterDeleteItemMatch = ({query, id}) => {
    return (cache, result) => {
        const mutationKey = Object.keys(result.data)[0];
        const deletedItem = result.data[mutationKey];

        const options = { query, variables: {id}};
    
        const data = cache.readQuery(options);

        for (let collection in data.viewer){
            if (isObject(data.viewer[collection])){
                let newItems = data.viewer[collection].matches.filter(item => item._id !== deletedItem._id);
                
                cache.writeQuery({
                    ...options,
                    data:{
                        viewer:{
                            ...data.viewer,
                            [collection]: {
                                ...data.viewer[collection],
                                matchCount: data.viewer[collection].matchCount - 1,
                                matches: newItems
                            }
                        }
                    }
                });
                break;
            }
        }
        
    }
}