export const isFunction = (varToCheck) => typeof varToCheck === "function";
export const isObject = (varToCheck) => varToCheck instanceof Object && varToCheck.constructor === Object;

// predicate should have signature (key, value) => ({ [newKey]: [newValue] })
export const mapObjectFields = (obj, predicate) => {
    if (!isObject(obj)){
        return obj;
    }

    return Object.entries(obj).reduce(function(acc, entry) {
        return {
            ...acc,
            ...predicate(entry[0], entry[1])
        };
    }, {});
}