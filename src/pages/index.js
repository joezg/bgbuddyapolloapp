import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../components/Home';
const ScorerPages = React.lazy(() => import('../modules/scorer/pages'));
const ScorerSidebar = React.lazy(() =>
  import('../modules/scorer/components/Sidebar')
);

export default ({ username }) => {
  return (
    <>
      <Route
        exact
        path="/"
        render={props => <Home username={username} {...props} />}
      />
      <Route path="/scorer" render={props => <ScorerPages {...props} />} />
    </>
  );
};

export const Sidebar = () => {
  return (
    <>
      <Route path="/scorer" render={props => <ScorerSidebar {...props} />} />
    </>
  );
};
