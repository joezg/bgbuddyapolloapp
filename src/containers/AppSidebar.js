import { withRouter } from 'react-router-dom';
import AppSidebar from '../components/common/AppSidebar';

export default withRouter(AppSidebar);
