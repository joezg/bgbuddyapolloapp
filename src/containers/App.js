import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { App } from '../components';

const BASIC_USER_DATA_QUERY = gql`
  query GetBasicUserData {
    viewer {
      username
      _id
    }
  }
`;

const AppContainer = props => (
  <Query query={BASIC_USER_DATA_QUERY}>
    {({ data }) => {
      const username = data && data.viewer ? data.viewer.username : null;

      return <App username={username} />;
    }}
  </Query>
);

export default AppContainer;
