import React, { Component } from 'react';
import { Container, Card, Icon } from 'semantic-ui-react';

class Home extends Component {
  render() {
    const name = this.props.username ? this.props.username : '...';
    const myMenu = [
      {
        headerText: 'Scorer',
        metaText: 'Add plays, view statistics',
        linkTo: 'scorer/matches',
        iconName: 'trophy',
        iconColor: 'olive'
      }
    ];

    return (
      <Container>
        <h1>Welcome {name}!</h1>
        <h3>What do you want to do today?</h3>
        <br />
        <Card.Group>
          {myMenu.map(menu => {
            return (
              <Card
                key={menu.headerText}
                link
                onClick={() => {
                  this.props.history.push(menu.linkTo);
                }}
              >
                <Card.Content>
                  <Icon
                    circular
                    name={menu.iconName}
                    size="big"
                    color={menu.iconColor}
                    style={{ float: 'right' }}
                  />
                  <Card.Header>{menu.headerText}</Card.Header>
                  <Card.Meta>{menu.metaText}</Card.Meta>
                </Card.Content>
              </Card>
            );
          })}
        </Card.Group>
      </Container>
    );
  }
}

export default Home;
