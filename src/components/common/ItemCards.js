import React, { Component } from 'react';
import { Card, Button, Container } from 'semantic-ui-react';
import { isFunction } from '../../helpers/general.helpers';
import ItemCard from './ItemCard';
import ItemCardsQuery from './ItemCardsQuery';
import styles from './ItemCards.module.scss';
import debounce from 'debounce';
import FloatingButton from './FloatingButton';

class ItemCards extends Component {
  constructor(props) {
    super(props);
    this.handleOnQueryChange = debounce(this.handleOnQueryChange, 500);
  }
  render() {
    return (
      <Container>
        {!this.props.hideSearch ? (
          <ItemCardsQuery
            onQueryChanged={this.handleOnQueryChange.bind(this)}
            query={this.props.query}
          />
        ) : null}
        {this.props.items.length > 0 ? (
          <Card.Group itemsPerRow={4} stackable className={styles.items}>
            {this.props.items.map(item => {
              let canDelete = true;
              if (this.props.canDelete || this.props.canDelete === false) {
                canDelete = isFunction(this.props.canDelete)
                  ? this.props.canDelete(item)
                  : this.props.canDelete;
              }

              return (
                <ItemCard
                  key={item[this.props.itemIdField || '_id']}
                  item={item}
                  context={this.props.context}
                  cardContentComponent={this.props.cardContentComponent}
                  hideActions={
                    isFunction(this.props.hideCardActions)
                      ? this.props.hideCardActions(item)
                      : this.props.hideCardActions
                  }
                  onDetailsClicked={this.props.onDetailsClicked}
                  onDeleteItemClick={this.handleDeleteMatchClick.bind(this)}
                  canDelete={canDelete}
                  actions={this.props.actions}
                  onSelectItemClick={this.props.onSelected}
                  idField={this.props.itemIdField}
                />
              );
            })}
          </Card.Group>
        ) : (
          <span>
            {this.props.noItemsMessage
              ? this.props.noItemsMessage
              : 'No items to show'}
          </span>
        )}
        {this.props.hasMore ? (
          <Button
            className={styles.loadMore}
            onClick={this.handleLoadMoreClick.bind(this)}
            fluid
          >
            Load more
          </Button>
        ) : null}
        {this.props.onNewItemClicked ? (
          <FloatingButton
            onClick={this.props.onNewItemClicked}
            iconName="plus"
          />
        ) : null}
      </Container>
    );
  }
  handleLoadMoreClick() {
    if (this.props.onLoadMoreClick) {
      this.props.onLoadMoreClick();
    }
  }
  async handleDeleteMatchClick(_id) {
    await this.props.onDeleteItemClick(_id);

    if (this.props.onLoadMoreClick) {
      this.props.onLoadMoreClick(1);
    }
  }
  handleOnQueryChange(query) {
    if (!this.props.onQueryChanged) {
      return;
    }
    this.props.onQueryChanged(query);
  }
}

export default ItemCards;
