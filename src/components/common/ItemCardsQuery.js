import React, { Component } from 'react';
import { Input, Button } from 'semantic-ui-react';

class ItemCardsQuery extends Component {
  constructor(props) {
    super(props);
    this.state = { query: props.query };
  }

  render() {
    return (
      <Input
        onChange={this.handleOnQueryChange.bind(this)}
        fluid
        icon="search"
        iconPosition="left"
        value={this.state.query}
        action={
          <Button icon="delete" onClick={this.handleClearQuery.bind(this)} />
        }
      />
    );
  }
  handleOnQueryChange = (event, data) => {
    this._changeQueryState(data.value);
  };

  handleClearQuery() {
    this._changeQueryState('');
  }

  _changeQueryState(query) {
    this.setState({
      query: query
    });
    this.props.onQueryChanged(query);
  }
}

export default ItemCardsQuery;
