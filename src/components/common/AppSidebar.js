import React, { Component, createRef } from 'react';
import { Sidebar, Menu, Ref, Container } from 'semantic-ui-react';
import styles from './AppSidebar.module.scss';

class AppSidebar extends Component {
  constructor(props) {
    super(props);

    this.contentRef = createRef();

    this.props.history &&
      this.props.history.listen((location, action) => {
        this.props.onMenuClose();
      });
  }
  render() {
    const SidebarMenu = this.props.sidebarMenu;
    return (
      <Sidebar.Pushable className={styles.appSidebar}>
        <Sidebar
          as={Menu}
          animation="overlay"
          icon="labeled"
          inverted
          onHide={this.props.onMenuClose}
          vertical
          visible={this.props.menuOpen}
          width="thin"
          target={this.contentRef.current}
          className={styles.sidebarContent}
        >
          <SidebarMenu />
        </Sidebar>

        <Sidebar.Pusher>
          <Ref innerRef={this.contentRef}>
            <Container className={styles.mainContainer}>
              {this.props.children}
            </Container>
          </Ref>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}

export default AppSidebar;
