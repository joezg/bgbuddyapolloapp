import React, { Component } from 'react';
import {
  Container,
  Form,
  Button,
  Header,
  Checkbox,
  Message
} from 'semantic-ui-react';
import BggSearchActionFormField from '../bgg/BggSearchActionFormField';
import { mapObjectFields } from '../../helpers/general.helpers';

class ActionForm extends Component {
  constructor(props) {
    super(props);

    const item = props.item || {};

    const fieldReducer = (acc, field) => {
      let initialValue = undefined;
      if (item.hasOwnProperty(field.name)) {
        initialValue = item[field.name];
      } else {
        switch (field.type) {
          case 'switch':
            initialValue = false;
            break;
          case 'id':
            initialValue = null;
            break;
          default:
            initialValue = '';
        }
      }

      acc[field.name] = initialValue;

      return acc;
    };

    this.state = {
      fields: props.fields.reduce(fieldReducer, {}),
      continue: false,
      result: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
    this.handleSwitchChange = this.handleSwitchChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderFields = this.renderFields.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.item !== nextProps.item) {
      this.setState(state => {
        const fields = { ...state.fields };

        for (let field in fields) {
          if (
            fields.hasOwnProperty(field) &&
            nextProps.item.hasOwnProperty(field)
          ) {
            fields[field] = nextProps.item[field];
          }
        }

        return { fields };
      });
    }
  }

  renderFields() {
    const state = this.state;
    return this.props.fields.map(field => {
      if (field.visible === undefined || field.visible(state.fields)) {
        switch (field.type) {
          case 'id':
            return null;
          case 'input':
            return (
              <Form.Field key={field.name}>
                <label>{field.label}</label>
                <input
                  type={field.inputType || 'text'}
                  value={this.state.fields[field.name]}
                  onChange={this.handleValueChange}
                  name={field.name}
                  placeholder={field.placeholder}
                />
              </Form.Field>
            );
          case 'switch':
            return (
              <Form.Field key={field.name}>
                <label>{field.label}</label>
                <Checkbox
                  toggle
                  checked={this.state.fields[field.name]}
                  onChange={this.handleSwitchChange}
                  name={field.name}
                />
              </Form.Field>
            );
          case 'bggSearch':
            return (
              <BggSearchActionFormField
                key={field.name}
                value={this.state.fields[field.name]}
                bggId={this.state.fields.bggId}
                onNameChange={name => this.handleChange(name, field.name)}
                onBggSelected={bggId => this.handleChange(bggId, 'bggId')}
                onBggSearchClicked={bggQuery =>
                  field.onBggSearchClicked(bggQuery)
                }
                name={field.name}
                searchResult={field.serachResult}
                placeholder={field.placeholder}
              />
            );
          default:
            return null;
        }
      } else {
        return null;
      }
    });
  }

  render() {
    return (
      <Container>
        <Header as="h3" block>
          {this.props.formHeader}
          {this.props.formSubheader && (
            <Header.Subheader>{this.props.formSubheader}</Header.Subheader>
          )}
        </Header>

        <Form onSubmit={this.handleSubmit} error={!!this.props.errorMessage}>
          {this.renderFields()}
          <Message
            error
            header={this.props.errorTitle}
            content={this.props.errorMessage}
          />
          <Button type="submit">{this.props.submitLabel || 'Save'}</Button>
        </Form>
      </Container>
    );
  }

  handleValueChange(evt) {
    this.handleChange(evt.target.value, evt.target.name);
  }

  handleSwitchChange(evt, { checked, name }) {
    this.handleChange(checked, name);
  }

  handleChange(newValue, name) {
    this.setState(state => {
      return {
        fields: {
          ...state.fields,
          [name]: newValue
        }
      };
    });
  }

  getFields() {
    return mapObjectFields(this.state.fields, (key, value) => {
      return { [key]: value === '' ? null : value };
    });
  }

  handleSubmit(evt) {
    evt.preventDefault();
    this.props.onSubmit && this.props.onSubmit(this.getFields());
  }
}

export default ActionForm;
