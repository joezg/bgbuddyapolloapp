import React, { Component } from 'react';
import { Card, Button } from 'semantic-ui-react';

class ItemCard extends Component {
  constructor(props) {
    super(props);

    this.handleOnDeleteItemClick = this.handleOnDeleteItemClick.bind(this);
    this.handleOnDetailsClick = this.handleOnDetailsClick.bind(this);
    this.handleOnSelectItemClick = this.handleOnSelectItemClick.bind(this);
  }

  render() {
    const { select = false, remove = true, details = true } = this.props.actions || {};

    return (
      <Card raised className={this.props.disabled ? "item-disabled": ""}>
        <this.props.cardContentComponent item={this.props.item} context={this.props.context} />
        {!this.props.hideActions ? (
          <Card.Content extra>
            {select ? <Button positive floated="right" icon="checkmark" onClick={this.handleOnSelectItemClick} /> : null }
            {remove ? <Button negative floated="right" icon="delete" disabled={!this.props.canDelete} onClick={this.handleOnDeleteItemClick} /> : null }
            {details ? <Button positive floated="right" icon="browser" onClick={this.handleOnDetailsClick} /> : null }
          </Card.Content> ) : null }
      </Card>
    );
  }

  handleOnDetailsClick = () => {
    this.props.onDetailsClicked(this.getId())
  }

  getId = () => {
    if (this.props.idField){
      return this.props.item[this.props.idField];
    } else {
      return this.props.item._id;
    }
  }

  handleOnSelectItemClick = (evt) => {
    this.props.onSelectItemClick(this.props.item);
    evt.preventDefault();
  }

  handleOnDeleteItemClick = (evt) => {
    this.props.onDeleteItemClick(this.getId());
    evt.preventDefault();
  }
}

export default ItemCard;

