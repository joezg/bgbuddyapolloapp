import React from 'react';
import { Button } from 'semantic-ui-react';
import styles from './FloatingButton.module.scss';

export default ({onClick, iconName}) => {
    return <Button className={styles.floatingButton} icon={iconName} positive circular size="massive" onClick={handleClick} />

    function handleClick(e){
        e.preventDefault();
        if (onClick){
            onClick();
        }
    }
}