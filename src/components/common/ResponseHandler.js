import React from 'react';
import Loader from './Loader';

export default ({ loading, error, children }) => {
  if (error) {
    console.error(error);
    return null;
  }

  return (
    <>
      {children}
      <Loader key="loader" loading={loading} />
    </>
  );
};
