import React from 'react';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import styles from './FloatingButton.module.scss';

export default ({iconName, location}) => {
    return <Link to={location}><Button className={styles.floatingButton} icon={iconName} positive circular size="massive" /></Link>
}