import React from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';

export default ({loading}) => {
    if (loading){
        return (
            <Dimmer active page >
                <Loader size="big">Loading</Loader>
            </Dimmer>
        ); 
    } else {
        return null;
    }
}