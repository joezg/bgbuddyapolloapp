import React, { Component, Suspense } from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Menu, Icon, Button } from 'semantic-ui-react';
import Pages, { Sidebar as PagesSidebar } from '../pages';

import styles from './App.module.scss';
import AppSidebar from '../containers/AppSidebar';
import Loader from './common/Loader';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuOpen: false
    };
  }

  handleMenuToggle = () => {
    this.setState(state => ({
      menuOpen: !state.menuOpen
    }));
  };

  handleMenuClose = () => {
    this.setState({
      menuOpen: false
    });
  };

  render() {
    return (
      <Router>
        <Suspense fallback={<Loader loading />}>
          <Menu inverted fixed="top" icon="labeled" className={styles.appMenu}>
            <Menu.Item as="a" name="gamepad" onClick={this.handleMenuToggle}>
              <Icon name="bars" />
            </Menu.Item>
            <Menu.Item header>
              <Link to="/">bgBuddy</Link>
            </Menu.Item>
            {this.props.username && (
              <Menu.Item position="right">
                <Button href="/logout">logout</Button>
              </Menu.Item>
            )}
          </Menu>
          <AppSidebar
            sidebarMenu={PagesSidebar}
            onMenuClose={this.handleMenuClose}
            menuOpen={this.state.menuOpen}
          >
            <Pages username={this.props.username} />
          </AppSidebar>
        </Suspense>
      </Router>
    );
  }
}

export default App;
