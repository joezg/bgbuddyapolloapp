import React, { Component } from 'react';
import { Card, Image } from 'semantic-ui-react';

class BggCardContent extends Component {
  render() {
    const item = this.props.item;
    
    return (
      <Card.Content>
        <Image floated='right' size='tiny' src={item.thumbnail} />
        <Card.Header>
          {item.name}
        </Card.Header>
        <Card.Meta>
          {item.year}
        </Card.Meta>
        <Card.Description>
          
        </Card.Description>
      </Card.Content>
    );
  }
}

export default BggCardContent;
