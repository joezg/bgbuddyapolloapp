import React, { Component } from 'react';
import BggSearch from './BggSearch';
import { Form } from 'semantic-ui-react';

class BggSearchActionFormField extends Component {
    constructor(props){
        super(props);

        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleBggSelected = this.handleBggSelected.bind(this);
    }
    render() {

        return (
            <Form.Field>
                <Form.Group>
                    <label>{this.props.label}</label>
                    <input 
                        value={this.props.value} 
                        onChange={this.handleOnChange} 
                        name={this.props.name} 
                        placeholder={this.props.placeholder} 
                    />
                    <Form.Button content="Search on BoardGameGeek" onClick={this.handleOnClick.bind(this)} />
                    
                </Form.Group>
                { this.props.searchResult && this.props.searchResult.length > 0 ? 
                    <BggSearch onSelected={this.handleBggSelected} result={this.props.searchResult} />
                    : null
                }
                { this.props.bggId ? "Item is selected" : null } 

            </Form.Field>
        );
    }

    handleBggSelected(bggItem){
        this.props.onBggSelected(bggItem.id);
        this.props.onNameChange(bggItem.name);
        this.props.onBggSearchClicked(null);
    }

    handleOnChange(e){
        this.props.onNameChange(e.target.value);
    }

    handleOnClick(e){
        this.props.onBggSearchClicked(this.props.value);
        e.preventDefault();
    }
};

export default BggSearchActionFormField;