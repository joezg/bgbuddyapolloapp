import React, { Component } from 'react';
import BggCardContent from './BggCardContent';
import ItemCards from '../common/ItemCards';

class BggSearch extends Component {
    constructor(props){
        super(props);
        this.handleSelected = this.handleSelected.bind(this);
    }

    render() {
        const result = this.props.result;

        return (
            <div>
                { result ? 
                    <ItemCards
                        items={result}
                        cardContentComponent={BggCardContent} 
                        actions={{ select: true, remove: false, details: false }}
                        hideSearch
                        onSelected={this.handleSelected}
                        itemIdField="id"
                    />
                    : null
                }
            </div>
        );
    }

    handleSelected(bggItem) {
        this.props.onSelected(bggItem);
    }
};

export default BggSearch;